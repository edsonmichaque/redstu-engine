
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.PathParams;

public class HttpPathTest {

    @Test
    public void url_must_be_deemed_to_not_match_when_it_contains_additional_chars_at_end_end(){
        HttpPath httpPath = new HttpPath("/user/{name}");
        Assertions.assertFalse(httpPath.matches("/user/mario/secrets"));
    }

    @Test
    public void must_return_empty_params_when_url_does_not_match(){
        HttpPath httpPath = new HttpPath("/user/{name}");
        Assertions.assertTrue(httpPath.getParams("/user/mario/secrets").isEmpty());
    }

    @Test
    public void name_and_id_params_must_be_found_in_url(){
        HttpPath httpPath = new HttpPath("/user/{name}/secrets/{id}");
        PathParams pathParams = httpPath.getParams("/user/mario/secrets/101").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("mario",pathParams.getValue("name"));
        Assertions.assertEquals("101",pathParams.getValue("id"));
    }

    @Test
    public void name_id_params_must_be_found_in_url(){
        HttpPath httpPath = new HttpPath("/user/{name}/secrets/{id}");
        PathParams pathParams = httpPath.getParams("/user/mario/secrets/101").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("mario",pathParams.getValue("name"));
        Assertions.assertEquals("101",pathParams.getValue("id"));
    }

    @Test
    public void url_encoded_param_must_be_matched(){
        HttpPath httpPath = new HttpPath("/countries/{countryId}/provinces/{provinceId}");
        PathParams pathParams = httpPath.getParams("/countries/MZ/provinces/Maputo%2DCity").orElseThrow(
                () -> new RuntimeException("url did not match"));
        Assertions.assertEquals("Maputo%2DCity",pathParams.getValue("provinceId"));
    }

}
