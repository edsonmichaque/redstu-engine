package org.mock.redstu.engine;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.proj.Deployment;
import org.mock.redstu.proj.Project;
import org.mock.redstu.server.config.CorsConfig;
import org.mock.redstu.server.config.ServerConfig;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Starter {

    private static final Logger LOGGER = LogManager.getLogger(Starter.class);

    public static void main(String[] args) throws Exception {
        Starter starter = new Starter();
        starter.deployProject();
    }

    private void deployProject(){
        Project project = getProject();
        Deployment deployment = project.createDeployment(getProjectServerConfig());
        deployment.bootstrap();
    }

    private Project getProject(){
        File projectDirectory = new File("".equals(SystemConfig.PROJECT_DIRECTORY.value()) ? new File("").getAbsolutePath() :
                SystemConfig.PROJECT_DIRECTORY.value());
        if(!projectDirectory.isDirectory()||!projectDirectory.exists()){
            throw new RuntimeException("Project directory does not exists: "+projectDirectory.getAbsolutePath());
        }

        if(!SystemConfig.PROJECT_DIRECTORY_MODIFY.asBoolean()){
            LOGGER.info("Copying project to temp directory");
            Path tempDirectoryPath = null;
            try {
                tempDirectoryPath = Files.createTempDirectory("redstu_");
            } catch (IOException e) {
                throw new RuntimeException("Error creating temporary directory for project");
            }
            File tempDirectory = tempDirectoryPath.toFile();
            try {
                FileUtils.copyDirectory(projectDirectory,tempDirectory);
            } catch (IOException ex) {
                throw new RuntimeException("Error copying project source to temp directory: "+tempDirectory.getAbsolutePath(),ex);
            }
            LOGGER.info("Project copied successfully");
            LOGGER.info("New project directory: "+tempDirectory.getAbsolutePath());
            projectDirectory = tempDirectory;
        }else LOGGER.info("Engine will work directly on project directory");

        return new Project(projectDirectory,SystemConfig.PROJECT_CONFIG_FILE.value());

    }

    private ServerConfig getProjectServerConfig(){
        CorsConfig corsConfig = new CorsConfig();
        corsConfig.setControlAllowHeaders(SystemConfig.PROJECT_SERVER_CORS_ALLOW_HEADERS.value());
        corsConfig.setControlAllowMethods(SystemConfig.PROJECT_SERVER_CORS_ALLOW_METHODS.value());
        corsConfig.setControlAllowOrigin(SystemConfig.PROJECT_SERVER_CORS_ALLOW_ORIGIN.value());
        corsConfig.setControlExposeHeaders(SystemConfig.PROJECT_SERVER_CORS_EXPOSE_HEADERS.value());
        corsConfig.setControlMaxAge(SystemConfig.PROJECT_SERVER_CORS_MAX_AGE.asInt());
        return new ServerConfig(SystemConfig.PROJECT_SERVER_HOSTNAME.value(),
                SystemConfig.PROJECT_SERVER_PORT.asInt(),
                corsConfig);
    }

}
