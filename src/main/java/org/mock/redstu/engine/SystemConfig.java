package org.mock.redstu.engine;

/**
 * System configurations enum
 */
public enum SystemConfig {

    PROJECT_DIRECTORY("PROJECT_DIR","project.dir.path",""),
    PROJECT_CONFIG_FILE("PROJECT_CONFIG_FILE","project.config.file","config.json"),
    PROJECT_SERVER_PORT("PROJECT_SERVER_PORT","project.server.port","8080"),
    PROJECT_SERVER_HOSTNAME("PROJECT_SERVER_HOSTNAME","project.server.hostname","localhost"),
    PROJECT_SERVER_CORS_ALLOW_METHODS("PROJECT_SERVER_CORS_ALLOW_METHODS","project.server.cors.allow.methods","*"),
    PROJECT_SERVER_CORS_ALLOW_HEADERS("PROJECT_SERVER_CORS_ALLOW_HEADERS","project.server.cors.allow.headers","*"),
    PROJECT_SERVER_CORS_EXPOSE_HEADERS("PROJECT_SERVER_CORS_EXPOSE_HEADERS","project.server.cors.expose.headers","*"),
    PROJECT_SERVER_CORS_MAX_AGE("PROJECT_SERVER_CORS_MAX_AGE","project.server.cors.max.age","86400"),
    PROJECT_SERVER_CORS_ALLOW_ORIGIN("PROJECT_SERVER_CORS_ALLOW_ORIGIN","project.server.cors.allow.origin","*"),
    ENGINE_API_SERVER_PORT("ENGINE_API_SERVER_PORT","engine.api.server.port","4849"),
    ENGINE_API_SERVER_HOSTNAME("ENGINE_API_SERVER_PORT","engine.api.server.hostname","localhost"),
    PROJECT_DIRECTORY_MODIFY("PROJECT_DIRECTORY_MODIFY","project.dir.modify","false"),
    ENGINE_API_ENABLE("ENGINE_API_ENABLE","engine.api.enable","false");

    private String environmentVariableName;
    private String systemPropertyKey;
    private String defaultValue;

    SystemConfig(String environmentVariableName, String systemPropertyKey, String defaultValue){
        this.environmentVariableName = environmentVariableName;
        this.systemPropertyKey = systemPropertyKey;
        this.defaultValue = defaultValue;
    }

    public String value(){
        String value = System.getProperty(systemPropertyKey);
        if(value==null||value.isEmpty())
            value = System.getenv(environmentVariableName);
        return (value!=null ? value : defaultValue);
    }

    public int asInt(){
        try{
            return Integer.parseInt(value());
        }catch (NumberFormatException ex){
            return Integer.parseInt(defaultValue);
        }
    }

    public boolean asBoolean(){
        return Boolean.parseBoolean(value());
    }

    public long asLong(){
        try{
            return Long.parseLong(value());
        }catch (NumberFormatException ex){
            return Long.parseLong(defaultValue);
        }
    }

}
