package org.mock.redstu.api.socket;

import java.util.Map;
import java.util.function.Consumer;

public interface WebSocket {

    void receive(Consumer<Map> consumer);
    void write(Map message);
    void close();

}
