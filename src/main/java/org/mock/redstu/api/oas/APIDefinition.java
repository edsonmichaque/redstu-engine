package org.mock.redstu.api.oas;

public interface APIDefinition {

    APIOperation getOperation(String id);

}
