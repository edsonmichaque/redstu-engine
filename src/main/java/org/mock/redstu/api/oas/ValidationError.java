package org.mock.redstu.api.oas;

public interface ValidationError {

    ReqPart getPart();
    String getKey();
    String getMessage();

}
