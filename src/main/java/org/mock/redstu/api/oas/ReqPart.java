package org.mock.redstu.api.oas;

public enum ReqPart {
    Header, Path, Body
}
