package org.mock.redstu.api.oas;

import org.mock.redstu.api.resp.HttpResp;

import java.util.List;

public interface ValidationResult {

    boolean anyErrors();
    List<ValidationError> getErrors();
    void sendErrors(HttpResp resp);
    void printErrors();

}
