package org.mock.redstu.api.oas;

import org.mock.redstu.api.req.HttpReq;

public interface APIOperation {
    
    void validate(HttpReq req);

}
