package org.mock.redstu.api.sse;

import org.mock.redstu.api.req.StringNameValues;

import java.util.function.Consumer;

public interface EventSession {

    String getId();
    EventSession emit(SseEvent event);
    StringNameValues pathParams();
    StringNameValues queryParams();
    boolean isClosed();
    void onClose(Consumer<EventSession> closeCallback);
    void close();

}
