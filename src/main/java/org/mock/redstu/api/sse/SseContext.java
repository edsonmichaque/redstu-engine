package org.mock.redstu.api.sse;

public interface SseContext {

    void broadcast(SseEvent event);
    EventSession[] getSessions();

}
