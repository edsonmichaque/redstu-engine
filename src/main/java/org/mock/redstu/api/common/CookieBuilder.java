package org.mock.redstu.api.common;

import java.time.Instant;

public class CookieBuilder {

    private HttpCookieImpl cookie;

    public CookieBuilder(String name, String value){
        this.cookie = new HttpCookieImpl(name,value);
    }

    public CookieBuilder expires(Instant expires){
        this.cookie.setExpires(expires);
        return this;
    }

    public CookieBuilder domain(String domain){
        this.cookie.setDomain(domain);
        return this;
    }

    public CookieBuilder path(String path){
        this.cookie.setPath(path);
        return this;
    }

    public CookieBuilder sameSiteMode(String mode){
        this.cookie.setSameSiteMode(mode);
        return this;
    }

    public CookieBuilder httpOnly(boolean httpOnly){
        this.cookie.setHttpOnly(httpOnly);
        return this;
    }

    public CookieBuilder secure(boolean secure){
        this.cookie.setSecured(secure);
        return this;
    }

    public CookieBuilder maxAge(int maxAge){
        this.cookie.setMaxAge(maxAge);
        return this;
    }

    public CookieBuilder comment(String comment){
        this.cookie.setComment(comment);
        return this;
    }

    public HttpCookie build(){
        return cookie;
    }

}
