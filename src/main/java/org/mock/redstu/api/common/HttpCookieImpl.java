package org.mock.redstu.api.common;

import io.undertow.server.handlers.Cookie;
import io.undertow.server.handlers.CookieImpl;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.resp.SameSiteMode;

import java.time.Instant;
import java.util.Date;

public class HttpCookieImpl implements HttpCookie {

    private String name;
    private String value;
    private Instant expires;
    private boolean secured;
    private boolean httpOnly;
    private String sameSiteMode = SameSiteMode.LAX;
    private String domain;
    private String path;
    private int maxAge = -1;
    private String comment;

    public HttpCookieImpl(Cookie cookie){
        this.name = cookie.getName();
        this.value = cookie.getValue();
        this.secured = cookie.isSecure();
        this.domain = cookie.getDomain();
        this.path = cookie.getPath();
        this.comment = cookie.getComment();
        this.sameSiteMode = cookie.getSameSiteMode();
        this.expires = cookie.getExpires().toInstant();
    }

    public HttpCookieImpl(String name, String value){
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public Instant getExpires() {
        return expires;
    }

    @Override
    public boolean isSecured() {
        return secured;
    }

    @Override
    public boolean isHttpOnly() {
        return httpOnly;
    }

    @Override
    public String getDomain() {
        return this.domain;
    }

    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public String getSameSiteMode() {
        return this.sameSiteMode;
    }

    @Override
    public int getMaxAge() {
        return maxAge;
    }

    public void setMaxAge(int maxAge) {
        this.maxAge = maxAge;
    }

    @Override
    public String getComment() {
        return this.comment;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setExpires(Instant expires) {
        this.expires = expires;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public void setHttpOnly(boolean httpOnly) {
        this.httpOnly = httpOnly;
    }

    public void setSameSiteMode(String mode) {
        this.sameSiteMode = mode;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
