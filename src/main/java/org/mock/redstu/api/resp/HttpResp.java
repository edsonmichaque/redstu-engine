package org.mock.redstu.api.resp;


import org.mock.redstu.api.common.HttpCookie;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;


public interface HttpResp {

    void setHeader(String name, String value);
    void setStatus(int status);
    void setContentType(String contentType);
    void setCookie(HttpCookie cookie);
    void sendBytes(ByteBuffer buffer);
    void sendArray(Object[] array);
    void sendObject(Map<String,Object> map);
    void sendText(String text);
    void sendText(String text, Charset charset);
    void send();
}
