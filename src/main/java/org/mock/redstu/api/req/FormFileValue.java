package org.mock.redstu.api.req;

import java.nio.ByteBuffer;

public interface FormFileValue extends FormValue {

    String filename();
    ByteBuffer content();

}
