package org.mock.redstu.api.req;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class StringNameValuesImpl implements StringNameValues {

    private Map<String,String> map = new HashMap<>();

    public StringNameValuesImpl(Map<String,String> values){
        this.map.putAll(values);
    }

    private void validateName(String name){
        if(name==null||name.isEmpty())
            throw new IllegalArgumentException("name must not be null nor empty");
    }

    @Override
    public Integer getInt(String name) {
        return getInt(name,null);
    }

    @Override
    public Integer getInt(String name, Integer def) {
        this.validateName(name);
        if(hasValue(name))
            return def;
        try {
            return Integer.parseInt(getString(name));
        }catch (NumberFormatException ex){
            return def;
        }
    }

    @Override
    public Long getBigInt(String name) {
        return getBigInt(name,null);
    }

    @Override
    public Long getBigInt(String name, Long def) {
        this.validateName(name);
        if(hasValue(name))
            return def;
        try {
            return Long.parseLong(getString(name));
        }catch (NumberFormatException ex){
            return def;
        }
    }

    @Override
    public Double getDecimal(String name) {
        return getDecimal(name,null);
    }

    @Override
    public Double getDecimal(String name, Double def) {
        this.validateName(name);
        if(hasValue(name))
            return def;
        try {
            return Double.parseDouble(getString(name));
        }catch (NumberFormatException ex){
            return def;
        }
    }

    @Override
    public String getString(String name) {
        return getString(name,null);
    }

    @Override
    public String getString(String name, String def) {
        validateName(name);
        if(hasValue(name))
            return def;
        return map.get(name);
    }

    @Override
    public String[] names() {
        String[] names = new String[map.size()];
        map.keySet().toArray(names);
        return names;
    }

    @Override
    public boolean hasValue(String name) {
        validateName(name);
        return !this.map.containsKey(name);
    }

    @Override
    public void forEach(BiConsumer<String, String> callback) {
        if(callback==null)
            throw new IllegalArgumentException("callback must not be null");
        this.map.forEach(callback);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public int size() {
        return map.size();
    }
}
