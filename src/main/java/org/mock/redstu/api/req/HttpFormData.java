package org.mock.redstu.api.req;

public interface HttpFormData {

    FormTextValue textEntry(String key);
    FormFileValue fileEntry(String key);
    FormFileValue[] filesEntry(String key);

}
