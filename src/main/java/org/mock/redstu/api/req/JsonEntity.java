package org.mock.redstu.api.req;

import java.util.Map;

public interface JsonEntity {

    String text();
    Map<String,Object> object();
    Object path(String jsonPath);

}
