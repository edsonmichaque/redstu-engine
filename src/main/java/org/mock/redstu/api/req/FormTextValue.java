package org.mock.redstu.api.req;

public interface FormTextValue extends FormValue {

    String value();
    Integer asInt();
    Long asBigInt();
    Double asDecimal();
    Boolean asBoolean();

}
