package org.mock.redstu.api.req;


public interface FormValue {

    String key();
    StringNameValues headers();

}
