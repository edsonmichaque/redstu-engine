package org.mock.redstu.api.req;

import java.util.Map;

public interface XmlEntity {

    String text();
    Map<String,Object> object();
    Object path(String xpath);

}
