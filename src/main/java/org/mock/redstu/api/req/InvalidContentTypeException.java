package org.mock.redstu.api.req;

public class InvalidContentTypeException extends IllegalStateException {

    public InvalidContentTypeException(String provided){
        super("Expected Content-Type doesn't match the provided: "+provided);
    }

}
