package org.mock.redstu.api.req;

import java.nio.ByteBuffer;
import java.util.function.BiConsumer;

public interface HttpReqBody {

    String getContentType();
    void binary(BiConsumer<ByteBuffer,RuntimeException> callback);
    void base64(BiConsumer<String,RuntimeException> callback);
    void text(BiConsumer<String,RuntimeException> callback);
    void json(BiConsumer<JsonEntity,RuntimeException> callback);
    void xml(BiConsumer<XmlEntity,RuntimeException> callback);
    void form(BiConsumer<HttpFormData,RuntimeException> callback);

}
