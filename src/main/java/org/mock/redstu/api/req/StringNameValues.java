package org.mock.redstu.api.req;


import java.util.function.BiConsumer;

public interface StringNameValues {

    Integer getInt(String name);
    Integer getInt(String name, Integer def);
    Long getBigInt(String name);
    Long getBigInt(String name, Long def);
    Double getDecimal(String name);
    Double getDecimal(String name, Double def);
    String getString(String name);
    String getString(String name, String def);
    String[] names();
    boolean hasValue(String name);
    void forEach(BiConsumer<String,String> callback);
    boolean isEmpty();
    int size();

}
