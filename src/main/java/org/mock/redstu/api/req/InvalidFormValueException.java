package org.mock.redstu.api.req;

public class InvalidFormValueException extends IllegalStateException {

    public static final String NOT_TEXT_MESSAGE = "Form value is not a text";
    public static final String NOT_FILE_MESSAGE = "Form value is not a file";

    public InvalidFormValueException(String message){
        super(message);
    }

}
