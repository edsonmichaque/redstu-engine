package org.mock.redstu.api.req;

import org.mock.redstu.api.common.HttpCookie;

public interface HttpReq {

    String getMethod();
    String getCharset();
    String getUrlPattern();
    String getUrl();
    String getRequestPath();
    String getQueryString();
    String getClientAddr();
    StringNameValues headers();
    StringNameValues queryParams();
    StringNameValues pathParams();
    HttpCookie[] cookies();
    HttpCookie cookie(String name);
    HttpReqBody body();

}
