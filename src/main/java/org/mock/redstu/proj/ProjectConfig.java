package org.mock.redstu.proj;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.server.JsonException;
import org.mock.redstu.util.JSON;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProjectConfig {

    private String templatesDir = "templates";
    private String xmlSchemasDir = "schemas/xml";
    private String jsonSchemasDir = "schemas/json";
    private List<String> javaExports = new ArrayList<>();

    private static final Logger LOGGER = LogManager.getLogger(ProjectConfig.class);

    public ProjectConfig(){

    }

    public static ProjectConfig loadFrom(File jsonFile){
        if(!jsonFile.exists()||jsonFile.isDirectory())
            throw new IllegalArgumentException("JSON file not found: "+jsonFile.getAbsolutePath());
        try {
            LOGGER.info("Loading project config file: "+jsonFile.getAbsolutePath());
            return JSON.parseBytesAs(FileUtils.readFileToByteArray(jsonFile), ProjectConfig.class);
        }catch (IOException ex){
            throw new RuntimeException("error load project config file: "+jsonFile.getAbsolutePath(),ex);
        }catch (JsonException ex){
            throw new RuntimeException("error parsing project config file: "+jsonFile.getAbsolutePath(),
                    ex);
        }
    }

    public String getTemplatesDir() {
        return templatesDir;
    }

    public String getXmlSchemasDir() {
        return xmlSchemasDir;
    }

    public String getJsonSchemasDir() {
        return jsonSchemasDir;
    }

    public List<String> getJavaExports() {
        return Collections.unmodifiableList(javaExports);
    }

    public void setTemplatesDir(String templatesDir) {
        if(templatesDir==null||templatesDir.isEmpty())
            throw new IllegalArgumentException("templatesDir must not be null nor empty");
        this.templatesDir = templatesDir;
    }

    public void setXmlSchemasDir(String xmlSchemasDir) {
        if(xmlSchemasDir==null||xmlSchemasDir.isEmpty())
            throw new IllegalArgumentException("xmlSchemasDir must not be null nor empty");
        this.xmlSchemasDir = xmlSchemasDir;
    }

    public void setJsonSchemasDir(String jsonSchemasDir) {
        if(jsonSchemasDir==null||jsonSchemasDir.isEmpty())
            throw new IllegalArgumentException("jsonSchemasDir must not be null nor empty");
        this.jsonSchemasDir = jsonSchemasDir;
    }

    public void setJavaExports(List<String> javaExports) {
        if(javaExports==null||javaExports.isEmpty())
            throw new IllegalArgumentException("javaExports must not be null");
        this.javaExports = javaExports;
    }
}
