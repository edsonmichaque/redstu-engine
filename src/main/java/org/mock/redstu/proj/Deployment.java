package org.mock.redstu.proj;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.mock.redstu.Expect;
import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.common.CookieBuilder;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.sse.EventBuilder;
import org.mock.redstu.api.sse.ServerSentEvents;
import org.mock.redstu.api.templating.Templates;
import org.mock.redstu.api.util.Freeze;
import org.mock.redstu.api.util.Postpone;
import org.mock.redstu.api.util.Repeat;
import org.mock.redstu.server.WebServer;
import org.mock.redstu.server.config.ServerConfig;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Deployment {

    private static Deployment CURRENT;

    private Project project;
    private ServerConfig serverConfig;
    private WebServer server;
    private Context context;
    private Map<String,Object> state = new HashMap<>();
    private OutputStream outputStream;

    private boolean boostraped = false;

    private static final Logger LOGGER = LogManager.getLogger(Deployment.class);
    private static final String JS_LANGUAGE_ID = "js";
    private static final String STATE_MEMBER_ID = "$$state";

    public Deployment(Project project, ServerConfig serverConfig){
        this.project = project;
        this.serverConfig = serverConfig;
    }

    public Map<String,Object> getState(){
        return state;
    }


    public WebServer getServer(){
        return server;
    }

    public void setOutputStream(OutputStream outputStream){
        if(outputStream==null)
            throw new IllegalArgumentException("outputStream must not be null");
        assertNotBootstrapped();
        this.outputStream = outputStream;
    }

    public void bootstrap(){
        assertNotBootstrapped();
        updateExportsFile();
        server = new WebServer(serverConfig);
        server.setCurrent();
        createJSContext();
        CURRENT = this;
        this.boostraped = true;
        server.start();
    }

    private void assertNotBootstrapped(){
        if(boostraped)
            throw new IllegalStateException("deployment already bootstrapped");
    }

    private String generateExports(){
        List<Class<?>> exportsList = Arrays.asList(String.class, Instant.class, TimeUnit.class, ByteBuffer.class, ByteOrder.class, Expect.class, MediaTypes.class,
                HttpCookie.class, CookieBuilder.class, ServerSentEvents.class,
                EventBuilder.class, Templates.class, Freeze.class, Postpone.class, Repeat.class);

        for(String export: project.getConfig().getJavaExports()){
            try {
                Class<?> clazz = Class.forName(export);
                exportsList.add(clazz);
            } catch (ClassNotFoundException e) {
                LOGGER.warn("Java export class not found: "+export);
            }
        }
        String[] exportNames = new String[exportsList.size()+1];
        exportNames[0] = "state";
        StringBuilder builder = new StringBuilder();
        builder.append(String.format("\nlet state=%s;",STATE_MEMBER_ID));
        int i = 1;
        for(Class<?> clazz: exportsList){
            exportNames[i] = clazz.getSimpleName();
            builder.append(String.format("\nvar %s = Java.type('%s');",clazz.getSimpleName(),clazz.
                    getCanonicalName()));
            i++;
        }
        builder.append(String.format("\nexport {%s};",String.join(", ",exportNames)));
        return builder.toString();
    }

    private void updateExportsFile(){
        try {
            FileUtils.writeStringToFile(project.getExportsFile(), generateExports(), Charset.forName("utf-8"));
        } catch (IOException ex) {
            throw new RuntimeException("error updating  exports file: "+project.getExportsFile().getAbsolutePath(),
                    ex);
        }
    }

    private void createJSContext(){
        Context.Builder contextBuilder =
                Context.newBuilder(JS_LANGUAGE_ID)
                .allowAllAccess(true)
                .currentWorkingDirectory(Paths.get(project.getSrcDirectory()
                        .getAbsolutePath()));
        if(outputStream!=null)
            contextBuilder.out(outputStream);
        context = contextBuilder.build();
        Value value = context.getBindings(JS_LANGUAGE_ID);
        value.putMember(STATE_MEMBER_ID,state);
        Source source = null;
        try {
            source = Source.newBuilder(JS_LANGUAGE_ID, project.getEntryFile())
                    .mimeType("application/javascript+module")
                    .build();
        } catch (IOException ex) {
            throw new RuntimeException("error loading project entry file: "+project.getEntryFile().getAbsolutePath(),
                    ex);
        }
        context.eval(source);
    }


    public static Deployment current(){
        return CURRENT;
    }

}
