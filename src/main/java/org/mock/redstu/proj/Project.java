package org.mock.redstu.proj;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.server.config.ServerConfig;

import java.io.File;

/**
 * Represents a dynamic mock project
 */
public class Project {

    private File srcDirectory;
    private String configFilePath;

    private ProjectConfig config;
    private File entryFile;
    private File exportsFile;

    private static final Logger LOGGER = LogManager.getLogger(Project.class);

    public Project(File srcDirectory){
        this(srcDirectory,ProjectConstants.DEFAULT_CONFIG_FILENAME);
    }

    public Project(File srcDirectory,  String configFilePath){
        if(srcDirectory==null)
            throw new IllegalArgumentException("srcDirectory must not be null");
        if(!srcDirectory.exists()||!srcDirectory.isDirectory())
            throw new IllegalArgumentException("project src directory not found: "+srcDirectory.getAbsolutePath());
        if(configFilePath==null||configFilePath.isEmpty())
            throw new IllegalArgumentException("configFilePath must not be null nor empty");
        this.srcDirectory = srcDirectory;
        File configFile = new File(srcDirectory.getAbsolutePath(),configFilePath);
        if(!configFile.exists()||configFile.isDirectory()){
            LOGGER.warn("Project config file not found: "+configFile.getAbsolutePath());
            LOGGER.info("Using default project configs");
            config = new ProjectConfig();
        }else {
            config = ProjectConfig.loadFrom(configFile);
        }
        entryFile = new File(srcDirectory.getAbsolutePath(),ProjectConstants.ENTRY_FILENAME);
        if(!entryFile.exists()||entryFile.isDirectory())
            throw new IllegalStateException("entry file not found: "+entryFile.getAbsolutePath());
        exportsFile = new File(srcDirectory.getAbsolutePath(),ProjectConstants.REDSTU_EXPORTS_FILENAME);
        if(!exportsFile.exists() || !exportsFile.isFile())
            throw new IllegalStateException("exports file not found in project: "+exportsFile.
                    getAbsolutePath());
    }

    public File getTemplate(String path){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    public File getXMLSchema(String path){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    public File getJsonSchema(String path){

        //TODO: Implement
        throw new UnsupportedOperationException();

    }

    public File getEntryFile(){
        return entryFile;
    }

    public File getExportsFile(){
        return exportsFile;
    }

    public File getSrcDirectory() {
        return srcDirectory;
    }

    public ProjectConfig getConfig() {
        return config;
    }

    public Deployment createDeployment(ServerConfig serverConfigs){
        return new Deployment(this,
                serverConfigs);
    }


}
