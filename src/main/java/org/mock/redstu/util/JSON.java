package org.mock.redstu.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mock.redstu.server.JsonException;

import java.io.IOException;
import java.util.Map;

public class JSON {

    private static ObjectMapper MAPPER = new ObjectMapper();

    public static void setMapper(ObjectMapper mapper){
        if(mapper==null)
            throw new IllegalArgumentException("mapper must not be null");
        MAPPER = mapper;
    }

    public static byte[] bytes(Object object){
        try {
            return MAPPER.writeValueAsBytes(object);
        }catch (JsonProcessingException ex){
            throw new JsonException("error serializing object to JSON byte array",ex);
        }
    }

    public static String string(Object object){
        try {
            return MAPPER.writeValueAsString(object);
        }catch (JsonProcessingException ex){
            throw new JsonException("error serializing object to JSON string",ex);
        }
    }

    public static Map<String,Object> parseBytes(byte[] bytes){
        try {
            return MAPPER.readValue(bytes, Map.class);
        }catch (IOException ex){
            throw new JsonException("error deserializing JSON byte array to Map",ex);
        }
    }

    public static Map<String,Object> parseString(String string){
        try {
            return MAPPER.readValue(string, Map.class);
        }catch (IOException ex){
            throw new JsonException("error deserializing JSON string array to Map",ex);
        }
    }

    public static <T> T parseBytesAs(byte[] bytes, Class<T> type){
        try {
            return MAPPER.readValue(bytes, type);
        }catch (IOException ex){
            throw new JsonException("error deserializing JSON string array to type: "+type.getCanonicalName(),
                    ex);
        }
    }

}
