package org.mock.redstu.server.util;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

public class QueryParamsHelper {

    public static Map<String,String> toStringMap(Map<String, Deque<String>> map){
        Map<String,String> result = new HashMap<>();
        for(String key : map.keySet()){
            result.put(key,map.get(key).getFirst());
        }
        return result;
    }

}
