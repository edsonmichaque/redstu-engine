package org.mock.redstu.server;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.server.impl.StubConfig;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class MockRequestsHandler implements HttpHandler {

    private List<StubConfig> stubConfigs = new ArrayList<>();

    private static final Logger LOGGER = LogManager.getLogger(MockRequestsHandler.class);

    protected void addStub(StubConfig stubConfig){
        if(stubConfig==null)
            throw new IllegalArgumentException("stubConfig must not be null");
        this.stubConfigs.add(stubConfig);
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        //TODO: Make CORS configurable
        exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_ORIGIN),"*");
        exchange.getResponseHeaders().add(HttpString.tryFromString(Constants.HEADER_ACCESS_CONTROL_ALLOW_METHODS),Constants.METHODS_ALL);
        if(exchange.getRequestMethod().equalToString("OPTIONS")){
            exchange.setStatusCode(StatusCodes.ACCEPTED);
            exchange.getResponseSender().close();
            return;
        }
        LOGGER.debug("Matching {} {}",exchange.getRequestMethod().toString().toUpperCase(),exchange.getRequestPath());
        for(StubConfig stubConfig: stubConfigs){
            if(stubConfig.matches(exchange)){
                LOGGER.debug("Stub type -> {}",stubConfig.getType());
                LOGGER.info("Handling request");
                Instant start = Instant.now();
                stubConfig.handleRequest(exchange);
                Instant finish = Instant.now();
                long timeElapsed = Duration.between(start, finish).toMillis();
                LOGGER.info("Request handled. Took {} ms",timeElapsed);
                return;
            }
        }
        LOGGER.warn("No stub configured. Responding with NOT FOUND");
        exchange.getResponseHeaders().put(Headers.STATUS, 404);
        exchange.setStatusCode(404);
        exchange.getResponseSender().close();
    }

}
