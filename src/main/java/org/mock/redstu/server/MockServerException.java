package org.mock.redstu.server;

public class MockServerException extends RuntimeException {

    public MockServerException(String message){
        super(message);
    }

    public MockServerException(String message, Throwable cause){
        super(message,cause);
    }

}
