package org.mock.redstu.server.stub.sse;

import io.undertow.server.handlers.sse.ServerSentEventConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.req.StringNameValues;
import org.mock.redstu.api.req.StringNameValuesImpl;
import org.mock.redstu.api.sse.SseEvent;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.MockEventStreamException;
import org.mock.redstu.util.JSON;

import java.io.IOException;
import java.util.*;
import java.util.function.Consumer;

public class EventSessionImpl implements EventSession {

    private ServerSentEventConnection connection;
    private StringNameValues queryParameters = null;
    private StringNameValues pathParameters = null;
    private List<Consumer<EventSession>> closeCallbackList = new ArrayList<>();

    private boolean implicitlyClosed = false;
    private boolean closeEventFired = false;

    private static final Logger LOGGER = LogManager.getLogger(EventSessionImpl.class);

    public EventSessionImpl(ServerSentEventConnection connection, Map<String,String> queryParameters, Map<String,String> pathParameters){
        this.connection = connection;
        this.queryParameters = new StringNameValuesImpl(queryParameters);
        this.pathParameters = new StringNameValuesImpl(pathParameters);
        this.connection.addCloseTask((con -> {
            LOGGER.debug("Connection of session {} has been closed",getId());
            EventSessionImpl.this.fireCloseCallback();
        }));
    }

    @Override
    public String getId() {
        return UUID.randomUUID().toString();
    }

    @Override
    public EventSession emit(SseEvent event) {
        assertNotClosed();
        String data = null;
        if(event.getData() instanceof String){
            data = (String) event.getData();
        }else JSON.string(event.getData());
        this.connection.send(data,event.getName(),event.getId(), new ServerSentEventConnection.EventCallback(){

            @Override
            public void done(ServerSentEventConnection serverSentEventConnection, String s, String s1, String s2) {
                LOGGER.debug("Event sent successfully");
            }

            @Override
            public void failed(ServerSentEventConnection serverSentEventConnection, String s, String s1, String s2, IOException e) {
                LOGGER.debug("Error sending event");
                EventSessionImpl.this.implicitlyClosed = true;
                EventSessionImpl.this.fireCloseCallback();
            }
        });
        return this;
    }

    @Override
    public StringNameValues pathParams() {
        return pathParameters;
    }

    @Override
    public StringNameValues queryParams() {
        return queryParameters;
    }

    @Override
    public boolean isClosed() {
        return !connection.isOpen() || implicitlyClosed;
    }

    @Override
    public void onClose(Consumer<EventSession> callback) {
        assertNotClosed();
        if(callback==null)
            throw new IllegalArgumentException("callback must not be null");
        this.closeCallbackList.add(callback);
    }

    @Override
    public void close() {
        assertNotClosed();
        try {
            this.connection.close();
        }catch (IOException ex){
            throw new MockEventStreamException("error closing session",ex);
        }
    }

    private void assertNotClosed(){
        if(!connection.isOpen())
            throw new IllegalArgumentException("session already closed");
    }

    private void fireCloseCallback(){
        if(closeEventFired)
            return;
        for(Consumer<EventSession> callback: closeCallbackList){
            callback.accept(this);
        }
        this.closeEventFired = true;
    }
}
