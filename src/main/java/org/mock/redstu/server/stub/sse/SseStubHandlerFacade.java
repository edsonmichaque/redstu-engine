package org.mock.redstu.server.stub.sse;

import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.stub.ReactionInput;

import java.util.function.Consumer;

public class SseStubHandlerFacade implements Consumer<ReactionInput> {

    private Consumer<EventSession> receiver;

    public SseStubHandlerFacade(Consumer<EventSession> receiver){
        this.receiver = receiver;
    }

    @Override
    public void accept(ReactionInput reactionInput) {
        SseStubInput sseStubInput = (SseStubInput) reactionInput;
        receiver.accept(sseStubInput.getSession());
    }
}
