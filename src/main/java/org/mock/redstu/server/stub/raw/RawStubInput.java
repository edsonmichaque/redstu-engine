package org.mock.redstu.server.stub.raw;

import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.stub.ReactionInput;

public class RawStubInput implements ReactionInput {

    private HttpReq req;
    private HttpResp resp;

    public RawStubInput(HttpReq req, HttpResp resp){
        this.req = req;
        this.resp = resp;
    }

    public HttpReq getReq() {
        return req;
    }

    public HttpResp getResp() {
        return resp;
    }
}
