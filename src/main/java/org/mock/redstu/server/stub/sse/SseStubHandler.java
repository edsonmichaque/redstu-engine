package org.mock.redstu.server.stub.sse;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.sse.ServerSentEventHandler;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.PathParams;
import org.mock.redstu.server.impl.commons.StubHandler;
import org.mock.redstu.server.MockServerException;
import org.mock.redstu.server.stub.ReactionInput;
import org.mock.redstu.server.util.QueryParamsHelper;

import java.util.function.Consumer;

public class SseStubHandler extends StubHandler {

    private ServerSentEventHandler serverSentEventHandler;

    public SseStubHandler(HttpPath path, Consumer<ReactionInput> reactor) {
        super(path, reactor);
        this.serverSentEventHandler = new ServerSentEventHandler((connection,eventId) -> {
            HttpPath httpPath = getPath();
            PathParams pathParams = httpPath.getParams(connection.getRequestURI()).orElseThrow(() -> new MockServerException("failed to match Request Path"));
            EventSessionImpl session = new EventSessionImpl(connection, QueryParamsHelper.toStringMap(connection.getQueryParameters()),pathParams.toMap());
            SseStubInput sseStubInput = new SseStubInput(session);
            SseStubHandler.this.getReaction(sseStubInput);
        });
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) throws Exception {
        serverSentEventHandler.handleRequest(exchange);
    }
}
