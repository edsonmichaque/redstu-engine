package org.mock.redstu.server.stub.sse;

import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.stub.ReactionInput;


public class SseStubInput implements ReactionInput {

    private EventSession session;

    public SseStubInput(EventSession eventSession){
        this.session = eventSession;
    }


    public EventSession getSession() {
        return session;
    }


}
