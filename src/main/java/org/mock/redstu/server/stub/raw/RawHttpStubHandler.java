package org.mock.redstu.server.stub.raw;

import io.undertow.server.HttpServerExchange;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.PathParams;
import org.mock.redstu.server.impl.commons.StubHandler;
import org.mock.redstu.server.MockServerException;
import org.mock.redstu.server.impl.req.HttpRequestImpl;
import org.mock.redstu.server.impl.resp.HttpRespImpl;
import org.mock.redstu.server.stub.ReactionInput;

import java.util.function.Consumer;

public class RawHttpStubHandler extends StubHandler {

    public RawHttpStubHandler(HttpPath path, Consumer<ReactionInput> reactor) {
        super(path, reactor);
    }

    @Override
    public void handleRequest(HttpServerExchange exchange) {
        HttpPath httpPath = getPath();
        PathParams pathParams = httpPath.getParams(exchange.getRequestPath()).orElseThrow(() -> new MockServerException("failed to match Request Path"));
        getReaction(new RawStubInput(new HttpRequestImpl(httpPath,pathParams,exchange),new HttpRespImpl(exchange)));
    }

}
