package org.mock.redstu.server.stub.raw;

import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.stub.ReactionInput;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class RawHttpStubHandlerFacade implements Consumer<ReactionInput> {

    private BiConsumer<HttpReq, HttpResp> handler;

    public RawHttpStubHandlerFacade(BiConsumer<HttpReq, HttpResp> handler){
        this.handler = handler;
    }

    @Override
    public void accept(ReactionInput reactionInput) {
        RawStubInput rawStubInput = (RawStubInput) reactionInput;
        this.handler.accept(rawStubInput.getReq(),rawStubInput.
                getResp());
    }

}
