package org.mock.redstu.server;

public final class Constants {

    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_TRANSFER_ENCODING = "Transfer-Encoding";
    public static final String CHUNKED_TRANSFER_ENCODING = "chunked";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    public static final String HEADER_ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    public static final String METHODS_ALL = "GET, OPTIONS, HEAD, PUT, POST";

}
