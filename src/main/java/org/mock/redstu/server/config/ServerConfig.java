package org.mock.redstu.server.config;

public class ServerConfig {

    private String hostname = "localhost";
    private int port =  8081;
    private CorsConfig corsConfig = new CorsConfig();

    public ServerConfig(){

    }

    public ServerConfig(String hostname, int port, CorsConfig corsConfig){
        this.setHostname(hostname);
        this.setPort(port);
        this.setCorsConfig(corsConfig);
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        if(hostname==null||hostname.isEmpty())
            throw new IllegalArgumentException("hostname must not be null nor empty");
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        if(port<=0)
            throw new IllegalArgumentException("port must be higher than zero");
        this.port = port;
    }

    public CorsConfig getCorsConfig() {
        return corsConfig;
    }

    public void setCorsConfig(CorsConfig corsConfig) {
        if(corsConfig==null)
            throw new IllegalArgumentException("corsConfig must not be null");
        this.corsConfig = corsConfig;
    }
}
