package org.mock.redstu.server.config;

public class CorsConfig {

    private String controlAllowMethods = "*";
    private String controlAllowHeaders;
    private int controlMaxAge = 86400;
    private String controlExposeHeaders;
    private String controlAllowOrigin = "*";

    public String getControlAllowMethods() {
        return controlAllowMethods;
    }

    public void setControlAllowMethods(String controlAllowMethods) {
        this.controlAllowMethods = controlAllowMethods;
    }

    public String getControlAllowHeaders() {
        return controlAllowHeaders;
    }

    public void setControlAllowHeaders(String controlAllowHeaders) {
        this.controlAllowHeaders = controlAllowHeaders;
    }

    public int getControlMaxAge() {
        return controlMaxAge;
    }

    public void setControlMaxAge(int controlMaxAge) {
        this.controlMaxAge = controlMaxAge;
    }

    public String getControlExposeHeaders() {
        return controlExposeHeaders;
    }

    public void setControlExposeHeaders(String controlExposeHeaders) {
        this.controlExposeHeaders = controlExposeHeaders;
    }

    public String getControlAllowOrigin() {
        return controlAllowOrigin;
    }

    public void setControlAllowOrigin(String controlAllowOrigin) {
        this.controlAllowOrigin = controlAllowOrigin;
    }
}
