package org.mock.redstu.server;

import io.undertow.Undertow;
import org.mock.redstu.server.config.ServerConfig;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.Verb;
import org.mock.redstu.server.impl.StubConfig;
import org.mock.redstu.server.impl.StubConfigType;
import org.mock.redstu.server.stub.ReactionInput;
import org.mock.redstu.server.stub.raw.RawHttpStubHandler;
import org.mock.redstu.server.stub.sse.SseStubHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class WebServer {

    private ServerConfig config;
    private Undertow undertow;
    private Map<String,Object> attachments = new HashMap<>();
    private MockRequestsHandler requestsHandler = new MockRequestsHandler();
    private boolean active = false;

    private static WebServer CURRENT = null;

    public WebServer(ServerConfig config){
        this.config = config;
        undertow = Undertow.builder()
                .addHttpListener(config.getPort(), config.getHostname())
                .setHandler(requestsHandler).build();
    }

    public void response(Verb verb, String path, Consumer<ReactionInput> handler){
        //TODO: Validate arguments
        HttpPath httpPath = new HttpPath(path);
        StubConfig stubConfig = new StubConfig(httpPath,StubConfigType.Raw,new RawHttpStubHandler(httpPath,
                handler),verb);
        requestsHandler.addStub(stubConfig);
    }

    public void response(String path, Consumer<ReactionInput> receiver){
        this.response(null,path,receiver);
    }

    public void eventSource(String path, Consumer<ReactionInput> receiver){
        //TODO: Validate arguments
        HttpPath httpPath = new HttpPath(path);
        StubConfig stubConfig = new StubConfig(httpPath,StubConfigType.EventSource, new SseStubHandler(httpPath,
                receiver));
        requestsHandler.addStub(stubConfig);
    }

    public void start(){
        if(active)
            throw new IllegalStateException("already started");
        this.undertow.start();
        this.active = true;
    }

    public void stop(){
        if(!active)
            throw new IllegalStateException("server is not active");
        this.undertow.stop();
        this.active = false;
    }

    public boolean isActive(){
        return this.active;
    }

    public void putAttachment(String key, Object object){
        this.attachments.put(key,object);

    }

    public Object getAttachment(String key){
        return this.attachments.get(key);
    }

    public  void setCurrent(){
        CURRENT = this;
    }

    public static WebServer current(){
        if(CURRENT==null)
            throw new IllegalStateException("there is no current web server");
        return CURRENT;
    };

}
