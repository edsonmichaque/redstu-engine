package org.mock.redstu.server.impl.resp;

import org.mock.redstu.server.MockServerException;

public class MockResponseException extends MockServerException {

    public MockResponseException(String message) {
        super(message);
    }

    public MockResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
