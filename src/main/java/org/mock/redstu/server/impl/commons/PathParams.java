package org.mock.redstu.server.impl.commons;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;

public class PathParams {

    private List<String> paramNames;
    private Matcher matcher;

    PathParams(List<String> paramNames, Matcher matcher){
        this.paramNames = paramNames;
        this.matcher = matcher;
    }

    public List<String> getNames(){
        return Collections.unmodifiableList(paramNames);
    }

    public String getValue(String name){
        return this.matcher.group(name);
    }

    public Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        for(String name: getNames()){
            map.put(name,getValue(name));
        }
        return map;
    }

}
