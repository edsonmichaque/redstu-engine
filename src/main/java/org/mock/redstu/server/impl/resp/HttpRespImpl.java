package org.mock.redstu.server.impl.resp;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import org.mock.redstu.api.common.HeaderValueList;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.server.Constants;
import org.mock.redstu.server.util.CookieHelper;
import org.mock.redstu.util.JSON;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

public class HttpRespImpl implements HttpResp {

    private HttpServerExchange exchange;

    public HttpRespImpl(HttpServerExchange exchange){
        this.exchange = exchange;
    }

    @Override
    public void sendBytes(ByteBuffer data) {
        this.mustHaveContentType();
        this.exchange.getResponseSender().send(data);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendArray(Object[] array) {
        this.mustHaveContentType();
        byte[] data = JSON.bytes(array);
        this.sendBytes(ByteBuffer.wrap(data));
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendObject(Map<String, Object> map) {
        this.mustHaveContentType();
        byte[] data = JSON.bytes(map);
        this.sendBytes(ByteBuffer.wrap(data));
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendText(String text) {
        this.mustHaveContentType();
        this.exchange.getResponseSender().send(text);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void sendText(String text, Charset charset) {
        this.mustHaveContentType();
        this.exchange.getResponseSender().send(text,charset);
        this.exchange.getResponseSender().close();
    }

    @Override
    public void send() {
        this.exchange.getResponseSender().close();
    }


    @Override
    public void setHeader(String name, String value) {
        this.exchange.getResponseHeaders().put(HttpString.tryFromString(name),
                value);
    }

    public void setHeader(String name, HeaderValueList headerValue) {
        this.exchange.getResponseHeaders().putAll(
                HttpString.tryFromString(name),
                headerValue.getValues());
    }

    @Override
    public void setStatus(int status) {
        this.exchange.setStatusCode(status);
    }


    @Override
    public void setContentType(String contentType) {
        this.setHeader(Constants.HEADER_CONTENT_TYPE,contentType);
    }

    @Override
    public void setCookie(HttpCookie cookie) {
        this.exchange.setResponseCookie(CookieHelper.toUndertow(cookie));
    }

    private void mustHaveContentType(){
       HeaderValues headerValues = exchange.getResponseHeaders().get(Headers.CONTENT_TYPE);
       if(headerValues==null||headerValues.isEmpty())
           throw new IllegalStateException("Content type header not set");
    }

}
