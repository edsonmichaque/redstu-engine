package org.mock.redstu.server.impl.req;

import io.undertow.server.handlers.form.FormData;
import org.mock.redstu.api.req.FormTextValue;

public class FormTextValueImpl extends FormValueImpl implements FormTextValue {

    public FormTextValueImpl(String key, FormData.FormValue formValue){
        super(key,formValue);
    }

    @Override
    public String value() {
        return internalFormValue().getValue();
    }

    @Override
    public Integer asInt() {
        try{
            return Integer.parseInt(value());
        }catch (NumberFormatException ex){
            return 0;
        }
    }

    @Override
    public Long asBigInt() {
        try{
            return Long.parseLong(value());
        }catch (NumberFormatException ex){
            return 0L;
        }
    }

    @Override
    public Double asDecimal() {
        try{
            return Double.parseDouble(value());
        }catch (NumberFormatException ex){
            return 0.0;
        }
    }

    @Override
    public Boolean asBoolean() {
        return Boolean.parseBoolean(value());
    }

}
