package org.mock.redstu.server.impl;

import io.undertow.server.HttpServerExchange;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.StubHandler;
import org.mock.redstu.server.impl.commons.Verb;

public class StubConfig {

    private Verb verb;
    private StubConfigType type = StubConfigType.Raw;
    private HttpPath path;
    private StubHandler handler;

    public StubConfig(HttpPath path, StubConfigType type, StubHandler stubHandler){
        this(path,type,stubHandler,null);
    }

    public StubConfig(HttpPath path, StubConfigType type, StubHandler stubHandler, Verb verb){
        this.path = path;
        this.type = type;
        this.handler = stubHandler;
        this.verb = verb;
    }

    public Verb getVerb() {
        return verb;
    }

    public StubConfigType getType() {
        return type;
    }

    public boolean matches(HttpServerExchange exchange){
        boolean pathMatches = this.path.matches(exchange.getRequestPath());
        if(!pathMatches)
            return false;
        if(verb!=null){
            Verb parsedVerb = Verb.valueOf(exchange.getRequestMethod()
                    .toString()
                    .toUpperCase());
            return (parsedVerb==verb);
        }
        return true;
    }

    public void handleRequest(HttpServerExchange httpServerExchange) throws Exception {
        this.handler.handleRequest(httpServerExchange);
    }

}
