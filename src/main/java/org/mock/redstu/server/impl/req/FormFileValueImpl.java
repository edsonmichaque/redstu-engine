package org.mock.redstu.server.impl.req;

import io.undertow.server.handlers.form.FormData;
import org.mock.redstu.api.req.FormFileValue;
import org.mock.redstu.server.MockServerException;

import java.io.IOException;
import java.nio.ByteBuffer;

public class FormFileValueImpl extends FormValueImpl implements FormFileValue {

    private FormData.FileItem fileItem;

    public FormFileValueImpl(String key, FormData.FormValue formValue) {
        super(key, formValue);
        this.fileItem = formValue.getFileItem();
    }

    @Override
    public String filename() {
        return internalFormValue().getFileName();
    }

    @Override
    public ByteBuffer content() {
        try {
            return ByteBuffer.wrap(fileItem.getInputStream().readAllBytes());
        }catch (IOException ex){
            throw new MockServerException("error reading file data",ex);
        }
    }

}
