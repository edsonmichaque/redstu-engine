package org.mock.redstu.server.impl;

public enum StubConfigType {
    WebSocket, EventSource, Raw
}
