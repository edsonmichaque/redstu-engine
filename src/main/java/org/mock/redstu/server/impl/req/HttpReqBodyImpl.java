package org.mock.redstu.server.impl.req;

import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.form.FormDataParser;
import io.undertow.server.handlers.form.FormParserFactory;
import io.undertow.util.HeaderMap;
import io.undertow.util.HeaderValues;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.req.*;
import org.mock.redstu.server.MockServerException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.function.BiConsumer;

public class HttpReqBodyImpl implements HttpReqBody {

    private HttpServerExchange exchange;
    private ContentType contentType = null;
    private String contentTypeHeader;

    private static final Logger LOGGER = LogManager.getLogger(HttpReqBodyImpl.class);

    public HttpReqBodyImpl(HttpServerExchange exchange){
        this.exchange = exchange;
        initialize();
    }

    private void initialize(){
        HeaderMap headerMap = exchange.getRequestHeaders();
        HeaderValues headerValues = headerMap.get("Content-Type");
        if(headerValues!=null){
            contentTypeHeader = exchange.getRequestHeaders().get("Content-Type").getFirst();
            LOGGER.info("Content type Header: "+ contentTypeHeader);
            contentType = ContentType.which(contentTypeHeader);
        }else LOGGER.warn("Content type header missing");
        LOGGER.info("Content type: "+contentType);
    }


    @Override
    public String getContentType() {
        return contentTypeHeader;
    }

    @Override
    public void binary(BiConsumer<ByteBuffer, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        exchange.getRequestReceiver().receiveFullBytes((ex,bytes) -> {
            LOGGER.info("Successfully read Request body bytes");
            callback.accept(ByteBuffer.wrap(bytes),null);
        },(ex,err) -> {
            String errMessage = "Error reading request body bytes";
            LOGGER.error(errMessage,err);
            callback.accept(null, new MockServerException(errMessage,err));
        });
    }

    @Override
    public void text(BiConsumer<String, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        if(contentType!=ContentType.TEXT)
            callback.accept(null,invalidContentTypeException());
        exchange.getRequestReceiver().receiveFullString((ex,str) -> {
            callback.accept(str,null);
        }, (ex,err) ->{
            String errMessage = "Error reading Request body full string";
            LOGGER.error(errMessage,err);
            callback.accept(null,new MockServerException(errMessage,err));
        }, Charset.forName(exchange.getRequestCharset()));
    }

    @Override
    public void base64(BiConsumer<String, RuntimeException> callback) {
        //TODO: Implement
        throw new UnsupportedOperationException();
    }


    @Override
    public void json(BiConsumer<JsonEntity, RuntimeException> callback) {
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

    @Override
    public void xml(BiConsumer<XmlEntity, RuntimeException> callback) {
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

    @Override
    public void form(BiConsumer<HttpFormData, RuntimeException> callback) {
        this.assertCallbackNotNull(callback);
        if(contentType!=ContentType.FORM_DATA_MULTIPART && contentType!=ContentType.FORM_DATA_URL_ENCODED)
            callback.accept(null,invalidContentTypeException());
        try {
            LOGGER.info("Parsing Form data payload");
            FormParserFactory.Builder builder = FormParserFactory.builder();
            final FormDataParser formDataParser = builder.build().createParser(exchange);
            if (formDataParser != null) {
                formDataParser.parse(new HttpHandler() {
                    @Override
                    public void handleRequest(HttpServerExchange httpServerExchange) {
                        io.undertow.server.handlers.form.FormData formData = exchange.getAttachment(FormDataParser.FORM_DATA);
                        callback.accept(new FormDataImp(formData),null);
                    }
                });
            }
        }catch (RuntimeException ex){
            callback.accept(null,ex);
        }catch (Exception ex){
            callback.accept(null,new MockServerException("error found parsing form data",
                    ex));
        }
    }

    private void assertCallbackNotNull(BiConsumer callback){
        if(callback==null)
            throw new IllegalArgumentException("callback must not be null");
    }

    private InvalidContentTypeException invalidContentTypeException(){
        return new InvalidContentTypeException(contentTypeHeader);
    }

    private enum ContentType {
        FORM_DATA_MULTIPART("multipart/form-data",true),
        FORM_DATA_URL_ENCODED("application/x-www-form-urlencoded",true),
        OCTET_STREAM("application/octet-stream"),
        TEXT("text/",true),
        APPLICATION_JSON("application/json"),
        APPLICATION_XML("application/xml"),
        OTHER("");

        private String value;
        private boolean matchStart = false;

        ContentType(String value){
            this(value,false);
        }

        ContentType(String value, boolean matchStart){
            this.value = value;
            this.matchStart = matchStart;
        }

        public boolean match(String value){
            if(!matchStart)
                return this.value.equalsIgnoreCase(value);
            else return value.startsWith(this.value);
        }

        public static ContentType which(String value){
            for(ContentType type: ContentType.values()){
                if(type.match(value))
                    return type;
            }
            return OTHER;
        }

    }





}
