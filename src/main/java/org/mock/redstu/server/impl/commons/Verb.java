package org.mock.redstu.server.impl.commons;

public enum Verb {
    GET, POST, PUT, PATCH, DELETE
}
