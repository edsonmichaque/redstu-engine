package org.mock.redstu.server.impl.commons;

import io.undertow.server.HttpHandler;
import org.mock.redstu.server.stub.ReactionInput;

import java.util.function.Consumer;

public abstract class StubHandler implements HttpHandler {

    private HttpPath path;
    private Consumer<ReactionInput> reactor;

    public StubHandler(HttpPath path, Consumer<ReactionInput> reactor){
        this.path = path;
        this.reactor = reactor;
    }

    public HttpPath getPath() {
        return path;
    }

    protected void getReaction(ReactionInput input){
        this.reactor.accept(input);
    }

}
