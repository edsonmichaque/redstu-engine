package org.mock.redstu.server.impl.req;

import io.undertow.server.HttpServerExchange;
import io.undertow.server.handlers.Cookie;
import io.undertow.util.HeaderMap;
import io.undertow.util.HeaderValues;
import io.undertow.util.HttpString;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.common.HeaderValueList;
import org.mock.redstu.api.common.HttpCookie;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.req.HttpReqBody;
import org.mock.redstu.api.req.StringNameValues;
import org.mock.redstu.api.req.StringNameValuesImpl;
import org.mock.redstu.server.impl.commons.HttpPath;
import org.mock.redstu.server.impl.commons.PathParams;
import org.mock.redstu.api.common.HttpCookieImpl;

import java.util.*;

public class HttpRequestImpl implements HttpReq {

    private HttpPath httpPath;
    private PathParams pathParams;
    private HttpServerExchange exchange;

    private transient Map<String, String> headers;
    private transient Map<String,String> queryParams;
    private HttpReqBody reqBody;

    private static final Logger LOGGER = LogManager.getLogger(HttpRequestImpl.class);

    public HttpRequestImpl(HttpPath path, PathParams pathParams, HttpServerExchange exchange){
        this.exchange = exchange;
        this.pathParams = pathParams;
        this.httpPath = path;
        HeaderMap headerMap = exchange.getRequestHeaders();
        HeaderValues headerValues = headerMap.get("Content-Type");
        if(headerValues!=null)
            this.reqBody = new HttpReqBodyImpl(exchange);
    }

    @Override
    public String getMethod() {
        return exchange.getRequestMethod().toString();
    }

    @Override
    public String getCharset() {
        return exchange.getRequestCharset();
    }

    @Override
    public String getUrlPattern() {
        return httpPath.getPath();
    }

    @Override
    public String getUrl() {
        return exchange.getRequestURL();
    }

    @Override
    public String getRequestPath() {
        return exchange.getRequestPath();
    }

    @Override
    public String getQueryString() {
        return exchange.getQueryString();
    }

    @Override
    public String getClientAddr() {
        //TODO: Consider reverse proxies
        return exchange.getConnection().getPeerAddress().toString();
    }

    @Override
    public StringNameValues headers() {
        if(headers!=null)
            return new StringNameValuesImpl(headers);
        headers = new HashMap<>();
        HeaderMap headerMap = exchange.getRequestHeaders();
        Collection<HttpString> headerNames = headerMap.getHeaderNames();
        for(HttpString name: headerNames){
            LOGGER.debug("Found Request header: {}",name);
            headers.put(name.toString(), headerMap.get(name).getLast());
        }
        return new StringNameValuesImpl(headers);
    }

    @Override
    public StringNameValues queryParams() {
        if(queryParams!=null)
            return new StringNameValuesImpl(queryParams);
        queryParams = new HashMap<>();
        Map<String,Deque<String>> exchangeQueryParams = exchange.getQueryParameters();
        Set<String> keySet = exchangeQueryParams.keySet();
        for(String key: keySet) {
            queryParams.put(key, exchangeQueryParams.get(key).
                    getFirst());
        }
        return new StringNameValuesImpl(queryParams);
    }

    @Override
    public StringNameValues pathParams() {
        return new StringNameValuesImpl(pathParams.toMap());
    }

    @Override
    public HttpCookie[] cookies() {
        List<HttpCookie> cookieList = new ArrayList<>();
        for(Cookie cookie : exchange.requestCookies()){
            cookieList.add(new HttpCookieImpl(
                    cookie));
        }
        HttpCookie[] cookies = new HttpCookie[cookieList.size()];
        cookieList.toArray(cookies);
        return cookies;
    }

    @Override
    public HttpCookie cookie(String name) {
        Cookie cookie = exchange.getRequestCookie(name);
        if(cookie!=null)
            return new HttpCookieImpl(cookie);
        return null;
    }

    @Override
    public HttpReqBody body() {
        return this.reqBody;
    }

}
