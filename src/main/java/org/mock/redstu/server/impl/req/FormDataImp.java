package org.mock.redstu.server.impl.req;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mock.redstu.api.req.FormFileValue;
import org.mock.redstu.api.req.FormTextValue;
import org.mock.redstu.api.req.HttpFormData;
import org.mock.redstu.api.req.InvalidFormValueException;

import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class FormDataImp implements HttpFormData {


    private io.undertow.server.handlers.form.FormData formData;

    private static final Logger LOGGER = LogManager.getLogger(FormDataImp.class);

    public FormDataImp(io.undertow.server.handlers.form.FormData data){
        this.formData = data;
    }

    @Override
    public FormTextValue textEntry(String key) {
        keyCheck(key);
        LOGGER.debug("Getting text form value: {}",key);
        Deque<io.undertow.server.handlers.form.FormData.FormValue> formValues = formData.get(key);
        if(formValues==null||formValues.size()==0) {
            LOGGER.warn("Form doesn't have any text value with key={}",key);
            return null;
        }
        if(formValues.size()>1||formValues.getFirst().isFileItem())
            throw new InvalidFormValueException(InvalidFormValueException.NOT_TEXT_MESSAGE);
        return new FormTextValueImpl(key,formValues.getFirst());
    }

    @Override
    public FormFileValue fileEntry(String key) {
        keyCheck(key);
        LOGGER.debug("Getting file form value: {}",key);
        Deque<io.undertow.server.handlers.form.FormData.FormValue> formValues = formData.get(key);
        if(formValues==null||formValues.size()==0) {
            LOGGER.warn("Form doesn't have any file value with key={}", key);
            return null;
        }
        io.undertow.server.handlers.form.FormData.FormValue formValue = formValues.getFirst();
        if (!formValue.isFileItem())
            throw new InvalidFormValueException(InvalidFormValueException.
                    NOT_FILE_MESSAGE);
        return new FormFileValueImpl(key,formValue);

    }

    @Override
    public FormFileValue[] filesEntry(String key) {
        keyCheck(key);
        LOGGER.debug("Getting file array form value: {}",key);
        Deque<io.undertow.server.handlers.form.FormData.FormValue> formValues = formData.get(key);
        if(formValues==null||formValues.size()==0) {
            LOGGER.warn("Form doesn't have any file array value with key={}", key);
            return null;
        }
        io.undertow.server.handlers.form.FormData.FormValue formValue = formValues.getFirst();
        if (!formValue.isFileItem())
            throw new InvalidFormValueException(InvalidFormValueException.
                    NOT_FILE_MESSAGE);
        LOGGER.debug("Found a total of {} files for key={}",formValues.size(),key);
        List<FormFileValue> fileValueList = new ArrayList<>();
        for(io.undertow.server.handlers.form.FormData.FormValue item: formValues){
            fileValueList.add(new FormFileValueImpl(key,item));
        }
        FormFileValue[] fileValues = new FormFileValue[fileValueList.size()];
        fileValueList.toArray(fileValues);
        return fileValues;
    }

    private void keyCheck(String key){
        if(key==null||key.isEmpty())
            throw new IllegalArgumentException("key must not be null nor empty");
    }

}
