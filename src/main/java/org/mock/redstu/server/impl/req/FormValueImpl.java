package org.mock.redstu.server.impl.req;

import io.undertow.server.handlers.form.FormData;
import io.undertow.util.HeaderMap;
import io.undertow.util.HttpString;
import org.mock.redstu.api.common.HeaderValueList;
import org.mock.redstu.api.req.FormValue;
import org.mock.redstu.api.req.StringNameValues;
import org.mock.redstu.api.req.StringNameValuesImpl;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FormValueImpl implements FormValue {

    private String key;
    private FormData.FormValue formValue;

    public FormValueImpl(String key, FormData.FormValue formValue){
        this.key = key;
        this.formValue = formValue;
    }

    protected FormData.FormValue internalFormValue(){
        return formValue;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public StringNameValues headers() {
        Map<String, String> map = new HashMap<>();
        HeaderMap headerMap = formValue.getHeaders();
        if(headerMap!=null){
            Collection<HttpString> httpStrings = headerMap.getHeaderNames();
            for (HttpString headerName: httpStrings) {
                map.put(headerName.toString(), headerMap.get(headerName).getLast());
            }
        }
        return new StringNameValuesImpl(map);
    }

}
