package org.mock.redstu.server;

public class MockEventStreamException extends MockServerException {

    public MockEventStreamException(String message) {
        super(message);
    }

    public MockEventStreamException(String message, Throwable cause) {
        super(message, cause);
    }

}
