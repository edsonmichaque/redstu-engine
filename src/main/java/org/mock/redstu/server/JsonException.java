package org.mock.redstu.server;

public class JsonException extends MockServerException {

    public JsonException(String message) {
        super(message);
    }

    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
