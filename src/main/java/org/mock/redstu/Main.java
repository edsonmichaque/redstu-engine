package org.mock.redstu;

import org.mock.redstu.api.MediaTypes;
import org.mock.redstu.api.util.Repeat;
import org.mock.redstu.api.sse.SseEvent;
import org.mock.redstu.server.WebServer;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


public class Main {

    public static void main(String[] args){

        /*
        WebServer server = new WebServer("localhost",8081);
        server.setCurrent();

        Signals.watch("name",(signal) ->{

        });

        Expect.getRequest("/get",(req, resp) -> {
            resp.setContentType(MediaTypes.TEXT_PLAIN);
            resp.sendText("Hi there");
        });

        Expect.postRequest("/post",(req, resp) -> {
            resp.setContentType(MediaTypes.TEXT_PLAIN);
            req.body().form((form,ex) -> {
                resp.sendText("Hi there: "+form.textEntry("name"));
            });
        });

        Expect.anyRequest("/any",(req, res) -> {

        });

        Expect.sseConnection("/events/{name}",(session -> {
            System.out.println("New SSE session: "+session.pathParams().get("name"));
            session.onClose(it -> {
                System.out.println("Session closed: "+it.getId());
            });
            AtomicInteger counter = new AtomicInteger();
            Repeat.every(5,TimeUnit.SECONDS,(repetition) -> {
                if(!session.isClosed()) {
                    session.emit(SseEvent.builder()
                            .name("warn")
                            .data("Event-" + counter.incrementAndGet())
                            .build()
                    );
                }else repetition.requestInterruption();
            });
        }));

        server.start();*/

    }

}
