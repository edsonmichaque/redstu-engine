package org.mock.redstu;

import org.jetbrains.annotations.Nullable;
import org.mock.redstu.api.req.HttpReq;
import org.mock.redstu.api.resp.HttpResp;
import org.mock.redstu.api.sse.EventSession;
import org.mock.redstu.server.impl.commons.Verb;
import org.mock.redstu.server.WebServer;
import org.mock.redstu.server.stub.raw.RawStubInput;
import org.mock.redstu.server.stub.sse.SseStubInput;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public final class Expect {

    private static void response(@Nullable Verb verb, String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        WebServer.current().response(verb,path,(reactionInput -> {
            RawStubInput input = (RawStubInput) reactionInput;
            biConsumer.accept(input.getReq(),input.getResp());
        }));
    }

    public static void anyRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(null,path,biConsumer);
    }

    public static void getRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(Verb.GET,path,biConsumer);
    }

    public static void postRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(Verb.POST,path,biConsumer);
    }

    public static void putRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(Verb.PUT,path,biConsumer);
    }

    public static void patchRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(Verb.PATCH,path,biConsumer);
    }

    public static void deleteRequest(String path, BiConsumer<HttpReq, HttpResp> biConsumer){
        response(Verb.DELETE,path,biConsumer);
    }

    public static void sseConnection(String path, Consumer<EventSession> receiver){
        WebServer.current().eventSource(path, reactionInput -> {
            receiver.accept(((SseStubInput) reactionInput).getSession());
        });
    }

    public static void signal(String name, Consumer<Object> handler){
        //TODO: Implement
        throw new UnsupportedOperationException();
    }

}
