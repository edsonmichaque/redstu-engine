
/**
 * Represents a list of strings
 */
class StringList {

    #constructor() {

    }

    /**
     * Adds a string value to the list
     * @param {String} value - the value to add
     */
    add(value);

    /**
     * Gets the length of the list
     * @returns {int}
     */
    size();

    /**
     * Gets the element at at specific position
     * @param {int} pos - zero index position
     * @returns {?String}
     */
    get(pos);

}



export { StringList };