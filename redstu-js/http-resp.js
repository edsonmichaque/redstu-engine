
import {HeaderValueList, HttpCookie} from "./http-base";

class HttpResp {

    /**
     * Sets a response Header
     * @param {string} name - the name of header
     * @param {string} value - header value
     */
    setHeader(name,value);

    /**
     * Sets the response status code
     * @param {int} code - the status code
     */
    setStatus(code);

    /**
     * Sets the response content type
     * @param {string} type - the content type to ser
     */
    setContentType(type);


    /**
     * Sets a cookie
     * @param {HttpCookie} cookie - cookie to set
     */
    setCookie(cookie);


    /**
     *
     * @param buffer
     */
    sendBytes(buffer);

    /**
     * Sends a JSON object array response
     * @param {Object[]} array - the object array
     */
    sendArray(array);

    /**
     * Sends a JSON object response
     * @param {Object | Object<string,Object>} obj - the object to be sent
     */
    sendObject(obj);

    /**
     * Sends a text response
     * @param {string} text - the text content
     * @param {string} [charset] - the text charset
     */
    sendText(text, charset);

    /**
     * Sends a response without a body
     */
    send();


}


export {HttpResp};