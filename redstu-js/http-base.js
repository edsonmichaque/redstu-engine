
import { Instant } from "./time";
import { StringList } from "./collections";

/**
 * Provides HTTP Media type constants
 */
class MediaTypes {
    static APPLICATION_JSON = "application/json";
    static APPLICATION_XML = "application/xml";
    static APPLICATION_OCTET_STREAM = "application/octet-stream";
    static TEXT_PLAIN = "text/plain";
    static TEXT_HTML = "text/html";
    static TEXT_EVENT_STREAM = "text/event-stream";
}


/**
 * Represents an Http Cookie
 */
class HttpCookie {
    /**
     * Gets the name of the Cookie
     * @returns {String}
     */
    getName();

    /**
     * Gets the cookie value
     * @returns {String}
     */
    getValue();

    /**
     * Returns the Instant on which the Cookies is set to expire
     * @returns {?Instant}
     */
    getExpires();

    /**
     * Checks whether cookie is secured
     * @returns {Boolean}
     */
    isSecured();

    /**
     * Checks whether cookie is restricted to Http only
     * @returns {Boolean}
     */
    isHttpOnly();

    /**
     * Gets the cookie domain
     * @returns {?String}
     */
    getDomain();

    /**
     * Gets the cookie path
     * @returns {?String}
     */
    getPath();

    /**
     * Gets the cookie Same Side mode
     * @returns {?String}
     */
    getSameSiteMode();

    /**
     * Gets the number of seconds set for the Cookie to expire
     * @returns {?int}
     */
    getMaxAge();

    /**
     * Gets the cookie comment
     * @returns {?String}
     */
    getComment();

    /**
     * Creates a new Cookie
     * @param {string} name - the cookie name
     * @param {string} value - the cookie value
     * @returns {HttpCookie}
     */
    static build(name, value);

    /**
     * Creates a new CookieBuilder
     * @param {string} name - the cookie name
     * @param {string} value - the cookie value
     * @returns {CookieBuilder}
     */
    static builder(name, value);

}


/**
 * Object to build new Cookie instances
 */
class CookieBuilder {

    /**
     * Creates a new instance
     * @param {String} name - the cookie name
     * @param {String} value - the cookie value
     */
    constructor(name,value) {

    }

    /**
     * Defines whe instant in which the cookie should expire
     * @param {Instant} instant - the instant to expire the cookie
     * @returns {CookieBuilder}
     */
    expires(instant);

    /**
     * Sets the cookie domain
     * @param {string} value - the domain to set
     * @returns {CookieBuilder}
     */
    domain(value);

    /**
     * Sets the cookie path
     * @param {string} value - the path to set
     * @returns {CookieBuilder}
     */
    path(value);

    /**
     * Sets the cookie same site mode
     * @param {string} mode - the same site mode to set
     * @returns {CookieBuilder}
     */
    sameSiteMode(mode);

    /**
     * Defines whether cookie access should be forbidden to JavaScript
     * @param {string} value - true to forbid JavaScript from accessing the cookie
     * @returns {CookieBuilder}
     */
    httpOnly(value);

    /**
     * Defines whether cookie should only be available for secured Requests
     * @param {string} value - true to restrict cookie to secured Requests
     * @returns {CookieBuilder}
     */
    secure(value);


    /**
     * Defines the number of seconds until the cookie expires
     * @param {int} value - number of seconds until the cookie expires
     * @returns {CookieBuilder}
     */
    maxAge(value);

    /**
     * Sets a comment for the cookie
     * @param {string} value - the comment to be set
     * @returns {CookieBuilder}
     */
    comment(value);


    /**
     * Create a Cookie with all the details set on the Builder
     * @returns {HttpCookie}
     */
    build();

}


export {MediaTypes, HttpCookie, CookieBuilder};

