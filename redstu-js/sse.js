
import {StringNameValues} from "./commons";

class EventSession {

    /**
     * Gets the event session Id
     * @returns {string}
     */
    getId();

    /**
     * Emits an event
     * @param {SseEvent} event - the event to be emit
     */
    emit(event);

    /**
     * Gets the connection request path params
     * @returns {StringNameValues}
     */
    pathParams();

    /**
     * Gets the connection query params
     * @returns {StringNameValues}
     */
    queryParams();

    /**
     * Checks whether a session is closed
     * @returns {boolean}
     */
    isClosed();

    /**
     * Adds a listener for the session closure
     * @param {function(EventSession)} closeCallback - the event handler function
     */
    onClose(closeCallback);

    /**
     * Closes the server-sent event session
     */
    close();

}

/**
 * Represents an event to be sent by the server via an event stream
 */
class SseEvent {

    /**
     * Gets the event Id
     * @returns {?string}
     */
    getId();

    /**
     * Gets the event data
     * @returns {object}
     */
    getData();

    /**
     * Gets the event name
     * @returns {?string}
     */
    getName();

    /**
     * Creates a new EventBuilder
     * @returns {EventBuilder}
     */
    static builder();

    /**
     * Creates a new event
     * @param {Object} object - the event data
     * @returns {SseEvent}
     */
    static withData(object);

}

/**
 * Builds a new Event
 */
class EventBuilder {

    /**
     * Sets the event Id
     * @param {string} value - the event id
     * @returns EventBuilder
     */
    id(value);

    /**
     * Sets the event data
     * @param {object} value - the event data
     * @returns EventBuilder
     */
    data(value);

    /**
     * Sets the event name
     * @param {string} value - the event name
     * @returns EventBuilder
     */
    name(value);

    /**
     * Builds the event based on the set attributes
     * @returns {SseEvent}
     */
    build();

}

class ServerSentEvents {

    /**
     * Gets the context of a configured event stream
     * @param {string} path - the event stream path
     * @returns {SseContext}
     */
    static context(path);

}

class SseContext {

    /**
     * Broadcasts an event to all active sessions
     * @param {SseEvent} event - the event to broadcast
     */
    broadcast(event);

    /**
     * Gets all active event sessions
     * @returns {EventSession[]}
     */
    getSessions();

}

export { EventSession, EventBuilder, SseContext, SseEvent, ServerSentEvents };