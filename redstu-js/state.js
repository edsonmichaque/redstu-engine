
class StateMap {

    /**
     * Puts an object
     * @param {String} key - the key to bind
     * @param {Object} object - the object to put
     */
    put(key,object);

    /**
     * Checks whether the map contains an object bound to a key
     * @param {String} key - the key to check
     * @returns {Object}
     */
    containsKey(key);

    /**
     * Gets the object that is mapped to a key
     * @param {string} key - the key mapped to the object
     * @returns {?Object}
     */
    get(key);

    /**
     * Iterates over each key/value pair
     * @param {function(string,Object)} consumer - the consumer function
     */
    forEach(consumer);

}

let state = new StateMap();

export {state};