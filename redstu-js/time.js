/**
 * TIme unit constants
 */
class TimeUnit {

    static NANOSECONDS =  new #TimeUnit();
    static MICROSECONDS = new #TimeUnit();
    static MILLISECONDS = new #TimeUnit();
    static SECONDS = new #TimeUnit();
    static MINUTES = new #TimeUnit();
    static HOURS = new #TimeUnit();
    static DAYS = new #TimeUnit();

    #constructor() {

    }

}

/**
 * Represents a time moment.
 */
class Instant {

    /**
     * Checks whether the current Instant is before another Instant.
     * @param {Instant} instant - the Instant to compare to
     * @returns {Boolean}
     */
    isBefore(instant);

    /**
     * Checks whether the current Instant is after another Instant.
     * @param {Instant} instant - the Instant to compare to
     * @returns {Boolean}
     */
    isAfter(instant);

    /**
     * Converts instant to epoch milliseconds
     * @returns {BigInt}
     */
    toEpochMilli();

    /**
     * Creates an Instant object instance that corresponds to the current moment
     * @returns {Instant}
     */
    static now();


    /**
     * Creates an Instant object instance from epoch milliseconds
     * @param {BigInt} millis - the milliseconds
     * @returns {Instant}
     */
    static ofEpochMilli(millis);

}

export {TimeUnit, Instant };