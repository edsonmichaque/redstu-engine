
import {TimeUnit} from "./time";

/**
 * Represents an error
 */
class RuntimeException {

    /**
     * Gets the error message
     * @returns {String}
     */
    getMessage();

    /**
     * Prints the error stack trace to the standard output
     */
    printStackTrace();

}

/**
 * Represents a map of strings
 */
class StringNameValues {

    #constructor() {
    }


    /**
     * Gets an int value
     * @param {string} name - the name of the int value
     * @param {int} [def] - the default value to return in case there is no value or the value is not a valid int
     * @returns {?int}
     */
    getInt(name, def);


    /**
     * Gets a bigInt value
     * @param {string} name - the name of the BigInt value
     * @param {BigInt} [def] - the default value to return in case there is no value or the value is not a valid int
     * @returns {?BigInt}
     */
    getBigInt(name, def);


    /**
     * Gets a float value
     * @param {string} name - the name of the float value
     * @param {float} [def] - the default value to return in case there is no value or the value is not a valid float
     * @returns {?float}
     */
    getDecimal(name, def);

    /**
     * Gets a string value
     * @param {string} name - the name of the float value
     * @param {string} [def] - the default value to return in case there is no value
     * @returns {?string}
     */
    getString(name, def);

    /**
     * Checks whether the map is empty
     * @returns {boolean}
     */
    isEmpty();

    /**
     * Gets the total count of path params
     * @returns {int}
     */
    size();

    /**
     * Checks whether the map contains a value mapped to a specific name
     * @param {string} name - the name to check
     * @returns {boolean}
     */
    hasName(name);

    /**
     * Iterates over the key/value pairs
     * @param {function(String,String)} callback - callback to receive each key/value pair
     */
    forEach(callback);

}


/**
 * A type safe enumeration for byte orders.
 */
class ByteOrder {

    /**
     * Constant denoting big-endian byte order.
     * @type {ByteOrder}
     */
    static BIG_ENDIAN = new #ByteOrder();

    /**
     * Constant denoting big-endian byte order.
     * @type {ByteOrder}
     */
    static LITTLE_ENDIAN = new #ByteOrder();

    #constructor() {
    }

}

/**
 * A byte buffer.
 */
class ByteBuffer {

    #constructor() {
    }

    /**
     * Tells whether or not this buffer is backed by an accessible byte array.
     * @returns {Boolean}
     */
    hasArray();

    /**
     * Gets the byte array that backs the buffer
     * @returns {Int8Array}
     */
    array();

    /**
     * Tells whether or not this byte buffer is direct.
     * @returns {Boolean}
     */
    isDirect();

    /**
     * Retrieves this buffer's byte order.
     * @returns {ByteOrder}
     */
    order();

    /**
     * Wraps a byte array into a buffer.
     * The new buffer will be backed by the given byte array; that is, modifications to the buffer will cause the array to be modified and vice versa. The new buffer's capacity will be array.length, its position will be offset, its limit will be offset + length, and its mark will be undefined. Its backing array will be the given array, and its array offset will be zero
     * @param {Int8Array} array - The array that will back the new buffer
     * @param {Number} [offset] - The offset of the subarray to be used; must be non-negative and no larger than array.length. The new buffer's position will be set to this value.
     * @param {Number} [length] - The length of the subarray to be used; must be non-negative and no larger than array.length - offset. The new buffer's limit will be set to offset + length.
     * @returns {ByteBuffer}
     */
    static wrap(array, offset, length);


}


/**
 * Represents a configured repetition
 */
class Repetition {
    /**
     * Requests the interruption of the Repetition
     */
    requestInterruption(){

    }
}

class Repeat {

    /**
     * Configures a function to be executed repeatedly
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     * @param {function(Repetition)} callback - function to be executed repeatedly
     */
    static every(time,unit, callback){

    }

}

class Delay {

    /**
     * Blocks the execution for a specific amount of time
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     */
    static until(time, unit){

    }

}

class Postpone {

    /**
     * Postpones the execution of a function by  specific amount of time
     * @param {Number} time - time amount
     * @param {TimeUnit} unit - time unit
     * @param {function} func - the function being postponed
     */
    static by(time, unit, func);

}

export { RuntimeException, StringNameValues, ByteOrder, ByteBuffer,  Repeat, Delay, Postpone };