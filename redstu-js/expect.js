
import {HttpReq} from "./http-req";
import {HttpResp} from "./http-resp";
import {SseEvent, EventSession} from "./sse";


class Expect {

    /**
     * Configures a reaction for any HTTP Request matching a path
     * @param {String} path - the path to listen for Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static anyRequest(path, handler);

    /**
     * Configures a reaction for HTTP GET Requests
     * @param {String} path - the path to listen for GET Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static getRequest(path, handler);

    /**
     * Configures a reaction for HTTP POST Requests
     * @param {String} path - the path to listen for POST Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static postRequest(path, handler);

    /**
     * Configures a reaction for HTTP PATCH Requests
     * @param {String} path - the path to listen for PATCH Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static patchRequest(path, handler);

    /**
     * Configures a reaction for HTTP DELETE Requests
     * @param {String} path - the path to listen for DELETE Requests
     * @param {function(HttpReq,HttpResp)} handler - The function to handle the Requests
     */
    static deleteRequest(path, handler);

    /**
     * Configures a server-sent events handler
     * @param {string} path - the path to listen for server-sent events connections
     * @param {function(EventSession)} handler - The function to handle the sessions
     */
    static sseConnection(path, handler);

    /**
     * Configures a signal handler
     * @param {string} name - the name of the signal to expect
     * @param {function(Object)} handler - the signal handler function
     */
    static signal(name, handler);

}

export {Expect}