
import { HttpCookie, HeadersMap, QueryParamsMap, PathParamsMap } from './http-base';
import { StringNameValues, ByteBuffer, RuntimeException } from './commons';

/**
 * Represents a form data entry
 */
class FormValue {

    /**
     * Gets the entry key
     * @returns {string}
     */
    key();


    /**
     * Gets the HTTP Headers
     * @returns {StringNameValues}
     */
    headers();

}

/**
 * Represents a text entry of a form data
 */
class FormTextValue extends FormValue {

    /**
     * Gets the text value
     * @returns {string}
     */
    value();

    /**
     * Parses the value to int
     * @returns {int}
     */
    asInt();

    /**
     * Parses the value to BigInt
     * @returns {BigInt}
     */
    asBigInt();

    /**
     * Parses the value to float
     * @returns {float}
     */
    asDecimal();

    /**
     * Parses the value to boolean
     * @returns {boolean}
     */
    asBoolean();

}

/**
 * Represents an uploaded file Part
 */
class FormFileValue extends FormValue {

    /**
     * Gets the filename
     * @returns {String}
     */
    filename();


    /**
     * Gets the file part binary content
     * @returns {ByteBuffer}
     */
    content();

}

/**
 * Represents a HTTP Form data
 */
class HttpFormData {

    /**
     * Gets a text form data value
     * @param {String} key - the form data field key
     * @returns {FormTextValue}
     */
    textEntry(key);

    /**
     * Gets a file data part
     * @param {String} key - the form data field key
     * @returns {FormFileValue}
     */
    fileEntry(key);

    /**
     * Gets an array of file data parts
     * @param {String} key - the form data field key
     * @returns {FormFileValue[]}
     */
    filesEntry(key);

}


class JsonEntity {



}

class XmlEntity {

}

class HttpReqBody {

    #constructor() {
    }

    /**
     * Gets the Request content type
     * @returns {string}
     */
    getContentType();

    /**
     * Gets the request body binary content
     * @param {function(ByteBuffer,RuntimeException)} callback
     */
    binary(callback);

    /**
     * Gets the request body binary content base64 encoded
     * @param {function(String,RuntimeException)} callback
     */
    base64(callback);

    /**
     * Gets the request body text content
     * @param {function(String,RuntimeException)} callback
     */
    text(callback);

    /**
     * Gets the request body JSON payload
     * @param {function(JsonEntity,RuntimeException)} callback
     */
    json(callback);


    /**
     * Gets the request body XML payload
     * @param {function(XmlEntity,RuntimeException)} callback
     */
    xml(callback);

    /**
     * Gets the request body Form data
     * @param {function(HttpFormData,RuntimeException)} callback
     */
    form(callback);

}


/**
 * Represents an Http Request
 */
class HttpReq {

    /**
     * Gets the request method
     * @returns {string}
     */
    getMethod();

    /**
     * Gets the Request charset
     * @returns {string}
     */
    getCharset();

    /**
     * Gets the url pattern matched
     * @returns {string}
     */
    getUrlPattern();

    /**
     * Gets the full request URL
     * @returns {string}
     */
    getUrl();

    /**
     * Gets the request path
     * @returns {string}
     */
    getRequestPath();

    /**
     * Gets the request query string
     * @returns {string}
     */
    getQueryString();

    /**
     * Gets the client address
     * @returns {string}
     */
    getClientAddr();

    /**
     * @returns {StringNameValues}
     */
    headers();

    /**
     * Gets the request query params map
     * @returns {StringNameValues}
     */
    queryParams();

    /**
     * Gets the request path params map
     * @returns {StringNameValues}
     */
    pathParams();

    /**
     * Gets the request cookies
     * @returns {HttpCookie[]}
     */
    cookies();

    /**
     * Gets a cookie by name
     * @param {string} name - the cookie name
     * @returns {?HttpCookie}
     */
    cookie(name);


    /**
     * Gets the request body
     * @returns {HttpReqBody}
     */
    body();
    
}

export {FormValue, FormTextValue, FormFileValue, HttpFormData, XmlEntity, JsonEntity, HttpReqBody, HttpReq};
