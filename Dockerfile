FROM ubuntu:20.04
RUN apt-get update && apt-get --assume-yes install wget
RUN wget https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-21.0.0.2/graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz
RUN mkdir /usr/graalvm && tar -xzf graalvm-ce-java11-linux-amd64-21.0.0.2.tar.gz -C /usr/graalvm
ENV PATH=/usr/graalvm/graalvm-ce-java11-21.0.0.2/bin/:$PATH
ENV JAVA_HOME=/usr/graalvm/graalvm-ce-java11-21.0.0.2
ENV REDSTU_HOME=/etc/redstu
ENV PROJECT_DIR=/var/project
ENV PROJECT_DIRECTORY_MODIFY=true
RUN mkdir $REDSTU_HOME
COPY target/libs $REDSTU_HOME/libs
COPY target/redstu.jar $REDSTU_HOME/redstu.jar
WORKDIR $REDSTU_HOME
ENTRYPOINT ["java","-jar","./redstu.jar"]