
import { state, Expect, MediaTypes } from './redstu.js';

console.log("Hello there");

state.put("website","https://redstu.org");

Expect.getRequest("/website",(req, resp) => {
    resp.setStatus(200);
    resp.setContentType(MediaTypes.TEXT_PLAIN);
    resp.sendText(state.get("website").toString());
});

Expect.getRequest("/get/{name}",(req, resp) => {
    console.log(req.pathParams().getString("name"));
    resp.setContentType(MediaTypes.APPLICATION_JSON);
    resp.sendObject(req.pathParams());
});

Expect.getRequest("/empty",(req, resp) => {
    resp.setStatus(422);
    resp.send();
});